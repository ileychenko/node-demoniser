path = require('path')
os = require('os')
exec  = require('child_process').exec
child_process = require('child_process')
sha1 =require('sha1')
fs = require('fs')
forever = require('forever-monitor')
colors = require('colors/safe')


tmpName = sha1 process.argv[1].toLowerCase()
tmpDir = os.tmpdir()



options = {
  outLogFile:path.join(__dirname,"OutFile.log")
  errLogFile:path.join(__dirname,"ErrFile.log")
  strStart:"Демон запущений"
  strStop:"Демон зупинений"
  strStatusTrue:"Демон парцюе"
  strStatusFalse:"Демон не парцюе"
  strIsRuning:"Демон вже запущено"
  tmpDir:os.tmpdir()
  pidDir:"/var/run"
  exitSignal:"SIGINT"
}
exports.options = options



start = ()->
  status (err, running)->
    if err
      console.error colors.red( "Помилка запуску !!!")
      console.error(error)
    else
      if running
        console.log(colors.blue options.strIsRuning)
      else
        fs.writeFile(path.join( options.pidDir, tmpName + ".sh"),
          "#!/bin/sh\n  #{path.join( process.argv[0] )}
           #{path.join( process.argv[1] )}
          monitor >
           #{options.outLogFile}
          2> #{options.errLogFile}",(err)->
            if err
              console.error colors.red( "Помилка запису - #{path.join( options.pidDir, tmpName + ".sh")}!!!")
              console.error err
            else
              fs.chmodSync(path.join( options.pidDir, tmpName + ".sh"), "0755")
              exec "start-stop-daemon -Sbv  --exec #{path.join( options.pidDir, tmpName + ".sh")}",(error, stdout, stderr)->
                if error
                  console.error colors.red( "Помилка запуску !!!")
                  console.error(error)
                else
                  console.log(colors.green options.strStart)
        )

start2 = ()->
  outFD = fs.openSync options.outLogFile, 'w'
  errFD = fs.openSync options.errLogFile, 'w'
  monitor = child_process.spawn path.join( process.argv[0] ), [path.join( process.argv[1] ),"monitor"], {
    stdio: ['ipc', outFD, errFD],
    detached: true
  }
  console.log(colors.green options.strStart)
  monitor.disconnect()
  process.exit()

monitor = ()->
#  console.log(process.pid.toString())
  pid = require('daemon-pid')(path.join( options.pidDir, tmpName + ".pid"))
#  fs.writeFile path.join( options.pidDir, tmpName + ".pid"),process.pid.toString(),()->
  fs.unlink path.join( options.pidDir, tmpName + ".pid"),(err)->
    pid.write (err)->
      if err
        console.error colors.red( "Помилка запуску !!!" )
        console.error err
      else
        child = new (forever.Monitor)( process.argv[1])
        child.start()

status = (callback)->
  pid = require('daemon-pid')(path.join( options.pidDir, tmpName + ".pid"))
  pid.running (err, running)->
    if err
      console.error colors.red("Помилка запиту !!!")
      console.error err
      callback err, running
    else
      if callback
        callback err, running
      else
        if running
          console.log(colors.green options.strStatusTrue)
        else
          console.log(colors.red options.strStatusFalse)

stop = (callback)->
  pid = require('daemon-pid')(path.join( options.pidDir, tmpName + ".pid"))
  pid2 = require('daemon-pid')(path.join( options.pidDir, tmpName + "W.pid"))
#  pid.kill 'SIGTERM', (err)->
  pid.kill 'SIGINT', (err)->
    if err
      console.error colors.red("Помилка зупинки !!!")
      console.error(err)
      if callback then callback(err)
    else
      pid.delete(()->)
      pid2.kill options.exitSignal, (err)->
        if err
          console.error colors.red("Помилка зупинки !!!")
          console.error(err)
          if callback then callback(err)
        else
          console.log(colors.green options.strStop)
          pid2.delete(()->)
          fs.unlink(path.join( options.pidDir, tmpName + ".sh"),(err)->)
          if callback then callback()

exports.run = (optionsL,callback)->
  if typeof optionsL == "function"
    callback = optionsL
  else
    options.outLogFile = optionsL.outLogFile if optionsL.outLogFile
    options.errLogFile = optionsL.errLogFile if optionsL.errLogFile
    options.strStart = optionsL.strStart if optionsL.strStart
    options.strStop = optionsL.strStop  if optionsL.strStop
    options.strStatusTrue = optionsL.strStatusTrue if optionsL.strStatusTrue
    options.strStatusFalse = optionsL.strStatusFalse if optionsL.strStatusFalse
    options.strIsRuning = optionsL.strIsRuning if optionsL.strIsRuning
    options.pidDir = optionsL.pidDir if optionsL.pidDir
    options.exitSignal = optionsL.exitSignal if optionsL.exitSignal
    options.on = optionsL.on if optionsL.on
    if optionsL.key
      tmpName = sha1 optionsL.key
  switch process.argv[2]
    when "start"
      start2()
    when "stop"
      stop()
    when "restart"
      stop (err)->
        if !err
          start()
    when "status"
      status()
    when "monitor"
      monitor()
    else
      pid = require('daemon-pid')(path.join( options.pidDir, tmpName + "W.pid"))
      fs.unlink path.join( options.pidDir, tmpName + "W.pid"),(err)->
        pid.write (err)->
          if err
            console.error colors.red( "Помилка запуску !!!" )
            console.error err
          else
            process.on 'SIGINT', ()->
              if options.on
                options.on ()->
                  process.exit(2)
              else
                process.exit(2)
            callback()
